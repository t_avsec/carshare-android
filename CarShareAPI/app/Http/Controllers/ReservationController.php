<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Transactions;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'car_id' => 'required',
            'user_id' => 'required',
            'from' => 'required',
            'to' => 'required'
        ]);

        $reservation = new Reservation();
        $reservation->car_id = $data['car_id'];
        $reservation->user_id = $data['user_id'];
        $reservation->from = $data['from'];
        $reservation->to = $data['to'];
        $reservation->save();

        return response()->json(['message' =>'success']);
    }

    public function show($user_id)
    {
        return response()->json(Reservation::all()->where('user_id', $user_id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
