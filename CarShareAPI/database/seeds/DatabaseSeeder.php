<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            ['name' => 'Šiška', 'image' => 'siska.png', 'lat' => 46.079985, 'long' => 14.487449],
            ['name' => 'Center', 'image' => 'center.png', 'lat' => 46.046564, 'long' => 14.501972],
            ['name' => 'Rudnik', 'image' => 'rudnik.png', 'lat' => 46.021444, 'long' => 14.537631]
        ]);

        DB::table('cars')->insert([
            ['brand' => 'Volkswagen', 'name' => 'Golf', 'description' => 'Budget and powerful option', 'image' => 'http://46.101.204.62/images/golf.png', 'pricePerHour' => 0.35],
            ['brand' => 'Nissan', 'name' => 'Leaf', 'description' => 'Nissan Leaf "LEAF" je električni avtomobil japonskega proizvajalca Nissan. Pojavil se je leta 2010 v ZDA in na Japonskem. Po pravilih EPA je uradni doseg vozil 121 kilometrov. Ekvivalenta poraba je 2,19 l/100km.', 'image' => 'http://46.101.204.62/images/leaf.png', 'pricePerHour' => 0.2],
            ['brand' => 'Mercedes', 'name' => 'A Class', 'description' => 'The Mercedes-Benz A-Class is a subcompact executive car (subcompact in its first two generations) produced by the German automobile manufacturer Mercedes-Benz.', 'image' => 'http://46.101.204.62/images/mercedes-a.png', 'pricePerHour' => 0.5],

        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
