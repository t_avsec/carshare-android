<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('throttle')->get('/locations', 'LocationController@index');
Route::middleware('throttle')->get('/cars', 'CarController@index');
Route::middleware('throttle')->post('/transaction', 'TransactionsController@store');
Route::middleware('throttle')->get('/transaction/{user_id}', 'TransactionsController@show');
Route::middleware('throttle')->get('/transaction/sum/{user_id}', 'TransactionsController@getUserAmount');

Route::middleware('throttle')->post('/reservation', 'ReservationController@store');
Route::middleware('throttle')->get('/reservation/{user_id}', 'ReservationController@show');
