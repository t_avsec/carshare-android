## Ideja
**Naslov seminarske naloge**: CarShare
<br>
**Člana ekipe:** 
<br>
63180044: Peter Timotej Avsec
<br>
63180079: Luka Dedić

----

Aplikacijo bi uporabniki uporabljali za namen izposoje avtomobilov (angl. car sharing), kjer si uporabniki lahko na različnih mestih izposojajo avtomobile za določen čas, po tem času pa jih morajo vrniti (lahko na drugo lokacijo). Aplikacija bi omogočala registracijo uporabnika, vpogled nad stanjem avtomobilov na določeni lokaciji, rezervacijo avtomobilov in stanja. Za prikaz različnih lokacij za izposojo bi uporabila Google Maps. Za izposojo avtomobila bo potrebno plačati določeno vsoto denarja, ki jo uporabnik lahko doda na svojem profilu.