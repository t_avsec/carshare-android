package emp.avsec.carshare;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CarAdapter extends RecyclerView.Adapter<CarHolder> {

    Context c;
    ArrayList<CarModel> models;
    private FirebaseAuth mAuth;

    public CarAdapter(Context c, ArrayList<CarModel> models) {
        this.c = c;
        this.models = models;
    }

    @NonNull
    @Override
    public CarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_row, null);
        mAuth = FirebaseAuth.getInstance();
        return new CarHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarHolder holder, int position) {
        holder.title.setText(models.get(position).getBrand() + " " + models.get(position).getName());
        holder.description.setText(models.get(position).getDescription());
        holder.pricePerHour.setText(String.format("%.2f € / h", models.get(position).getPricePerHour()));
        Picasso.get().load(models.get(position).getImage()).into(holder.imageView);

        final int car_id = models.get(position).getId();
        final float price = models.get(position).getPricePerHour();
        final String user_id = mAuth.getUid();

        holder.reserveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ReservationFragment fragment = ReservationFragment.newInstance(car_id, user_id, price);
                ((AppCompatActivity)c).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }
}
