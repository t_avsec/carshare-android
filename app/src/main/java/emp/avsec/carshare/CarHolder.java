package emp.avsec.carshare;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CarHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView title, pricePerHour, description;
    Button reserveButton;

    public CarHolder(@NonNull View itemView) {
        super(itemView);

        this.imageView = itemView.findViewById(R.id.image);
        this.title = itemView.findViewById(R.id.name);
        this.pricePerHour = itemView.findViewById(R.id.pricePerHour);
        this.description = itemView.findViewById(R.id.description);
        this.reserveButton = itemView.findViewById(R.id.reserveButton);
    }
}
