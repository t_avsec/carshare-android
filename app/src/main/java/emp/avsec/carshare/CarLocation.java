package emp.avsec.carshare;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CarLocation {
    String name;
    double latitude;
    double longitude;
    String image;

    CarLocation(){}

    CarLocation(String name, double latitude, double longitude, String image){
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.image = image;
    }

    public static ArrayList<CarLocation> getCarLocations(){
        ArrayList<CarLocation> carLocations = new ArrayList<CarLocation>();
        carLocations.add(new CarLocation("Šiška", 46.079985, 14.487449, "siska.png"));
        carLocations.add(new CarLocation("Center", 46.046564, 14.501972, "center.png"));
        carLocations.add(new CarLocation("Rudnik", 46.021444, 14.537631, "rudnik.png"));
        return carLocations;
    }
}
