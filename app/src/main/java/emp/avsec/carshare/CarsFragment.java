package emp.avsec.carshare;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CarsFragment extends Fragment {
    private RequestQueue requestQueue;
    RecyclerView recyclerView;
    CarAdapter carAdapter;
    ArrayList<CarModel> cars = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        requestQueue = Volley.newRequestQueue(getActivity());
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View carsView = inflater.inflate(R.layout.fragment_cars, container, false);

        show_cars(carsView.getContext());

        recyclerView = carsView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(carsView.getContext()));

        return carsView;
    }

    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                try {
                    CarModel m = new CarModel();
                    JSONObject object = response.getJSONObject(i);

                    m.setId(object.getInt("id"));
                    m.setBrand(object.getString("brand"));
                    m.setName(object.getString("name"));
                    m.setDescription(object.getString("description"));
                    m.setPricePerHour((float) object.getDouble("pricePerHour"));
                    m.setImage(object.getString("image"));

                    cars.add(m);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
            }

            carAdapter = new CarAdapter(getContext(), cars);
            recyclerView.setAdapter(carAdapter);

        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("REST error", error.getMessage());
        }
    };

    public void show_cars(Context c) {
        String url = "http://46.101.204.62/api/cars";
        JsonArrayRequest request = new JsonArrayRequest(url, jsonArrayListener, errorListener);
        requestQueue.add(request);

    }
}
