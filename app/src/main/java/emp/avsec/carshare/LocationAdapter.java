package emp.avsec.carshare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class LocationAdapter extends RecyclerView.Adapter<LocationHolder> {

    Context c;
    ArrayList<LocationModel> models;

    public LocationAdapter(Context c, ArrayList<LocationModel> models) {
        this.c = c;
        this.models = models;
    }

    @NonNull
    @Override
    public LocationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_row, null);
        return new LocationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationHolder holder, int position) {
        final double latitude = models.get(position).getLatitude();
        final double longitude = models.get(position).getLongitude();

        holder.title.setText(models.get(position).getName());
        holder.latitude.setText(String.format("Lat: %f", latitude));
        holder.longitude.setText(String.format("Long: %f", longitude));
        holder.imageView.setImageResource(models.get(position).getImg());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String daddr = latitude + "," + longitude;
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=My+Location&daddr="+daddr));
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return models.size();
    }
}
