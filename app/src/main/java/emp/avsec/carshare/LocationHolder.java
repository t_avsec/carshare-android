package emp.avsec.carshare;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LocationHolder extends RecyclerView.ViewHolder {

    RelativeLayout layout;
    ImageView imageView;
    TextView title, latitude, longitude;

    public LocationHolder(@NonNull View itemView) {
        super(itemView);

        this.imageView = itemView.findViewById(R.id.image);
        this.title = itemView.findViewById(R.id.title);
        this.latitude = itemView.findViewById(R.id.latitude);
        this.longitude = itemView.findViewById(R.id.longitude);
        this.layout = itemView.findViewById(R.id.locationRowMain);

    }
}
