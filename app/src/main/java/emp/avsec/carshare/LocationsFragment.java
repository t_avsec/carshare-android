package emp.avsec.carshare;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LocationsFragment extends Fragment {
    private RequestQueue requestQueue;
    RecyclerView recyclerView;
    LocationAdapter locationAdapter;
    ArrayList<LocationModel> locationModels;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        requestQueue = Volley.newRequestQueue(getActivity());
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View locationsView = inflater.inflate(R.layout.fragment_locations, container, false);

        show_locations(locationsView.getContext());

        recyclerView =  locationsView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(locationsView.getContext()));


        return locationsView;
    }


    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            locationModels = new ArrayList<>();
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                try {
                    LocationModel m = new LocationModel();
                    JSONObject object = response.getJSONObject(i);

                    m.setName(object.getString("name"));
                    m.setLongitude(object.getDouble("long"));
                    m.setLatitude(object.getDouble("lat"));
                    m.setImg(R.drawable.common_google_signin_btn_icon_dark);

                    locationModels.add(m);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
            }

            locationAdapter = new LocationAdapter(getContext(), locationModels);
            recyclerView.setAdapter(locationAdapter);

        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("REST error", error.getMessage());
        }
    };

    public void show_locations(Context c) {
        String url = "http://46.101.204.62/api/locations";
        JsonArrayRequest request = new JsonArrayRequest(url, jsonArrayListener, errorListener);
        requestQueue.add(request);

    }
}
