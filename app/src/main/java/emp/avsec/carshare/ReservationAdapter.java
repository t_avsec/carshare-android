package emp.avsec.carshare;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationHolder> {
    Context c;
    ArrayList<ReservationModel> models;

    public ReservationAdapter(Context c, ArrayList<ReservationModel> models) {
        this.c = c;
        this.models = models;
    }

    @NonNull
    @Override
    public ReservationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_row, null);
        return new ReservationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationHolder holder, int position) {
        String title = "#" + models.get(position).getId() + " | ID avtomobila: " + models.get(position).getCar_id();
        holder.title.setText(title);
        holder.startDate.setText("Začetek: " + models.get(position).getStartDate());
        holder.endDate.setText("Začetek: " + models.get(position).getEndDate());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }
}
