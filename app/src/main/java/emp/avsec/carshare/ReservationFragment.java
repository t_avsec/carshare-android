package emp.avsec.carshare;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;


abstract class TextChangedListener<T> implements TextWatcher {
    private T target;

    public TextChangedListener(T target) {
        this.target = target;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        this.onTextChanged(target, s);
    }

    public abstract void onTextChanged(T target, Editable s);
}
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ReservationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReservationFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String CAR_ID = "car_id";
    private static final String USER_ID = "user_id";
    private static final String CAR_PRICE = "car_price";

    private int car_id;
    private String user_id;
    private float car_price;

    TextView price;
    EditText fromDate, toDate;
    Button confirmBtn;

    LocalDate dateStart;
    LocalDate dateEnd;

    private RequestQueue requestQueue;



    public ReservationFragment() {
        // Required empty public constructor
    }

    public static ReservationFragment newInstance(int car_id, String user_id, float car_price) {
        ReservationFragment fragment = new ReservationFragment();
        Bundle args = new Bundle();
        args.putInt(CAR_ID, car_id);
        args.putString(USER_ID, user_id);
        args.putFloat(CAR_PRICE, car_price);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            car_id = getArguments().getInt(CAR_ID);
            user_id = getArguments().getString(USER_ID);
            car_price = getArguments().getFloat(CAR_PRICE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_reservation, container, false);
        price = v.findViewById(R.id.price);
        fromDate = v.findViewById(R.id.fromInput);
        toDate = v.findViewById(R.id.toInput);
        confirmBtn = v.findViewById(R.id.submitButton);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addReservation(view);
            }
        });

        requestQueue = Volley.newRequestQueue(v.getContext());

        fromDate.addTextChangedListener(new TextChangedListener<EditText>(fromDate) {
            @Override
            public void onTextChanged(EditText target, Editable s) {
                try {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    dateStart = LocalDate.parse(s.toString(), formatter);
                    Duration diff = Duration.between(dateStart.atStartOfDay(), dateEnd.atStartOfDay());
                    price.setText(String.format("Cena: %.2f €", diff.toDays()  * car_price * 24));
                }catch (Exception e){
                    price.setText("Cena: -");
                }
            }
        });

        toDate.addTextChangedListener(new TextChangedListener<EditText>(fromDate) {
            @Override
            public void onTextChanged(EditText target, Editable s) {
                try {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    dateEnd = LocalDate.parse(s.toString(), formatter);
                    Duration diff = Duration.between(dateStart.atStartOfDay(), dateEnd.atStartOfDay());
                    price.setText(String.format("Cena: %.2f €", diff.toDays() * car_price * 24));
                }catch (Exception e){
                    price.setText("Cena: -");
                }
            }
        });
        return v;
    }

    public void addReservation(View view) {
        String userId = user_id;


        Duration diff = Duration.between(dateStart.atStartOfDay(), dateEnd.atStartOfDay());
        if(diff.toDays() < 0)
            return;


        if(((MainLoggedActivity)getActivity()).userCredits < diff.toDays() * car_price * 24){
            Toast.makeText(view.getContext(), "Nimate dovolj dobroimetja!", Toast.LENGTH_LONG).show();
            return;
        }

        addTransaction(view, -(diff.toDays() * car_price * 24));

        String URL = "http://46.101.204.62/api/reservation";
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("user_id", userId);
            jsonBody.put("car_id", car_id);
            jsonBody.put("from", dateStart.toString());
            jsonBody.put("to", dateEnd.toString());

            final String mRequestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("LOG_VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG_VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    ((MainLoggedActivity)getActivity()).getUserAmount();
                    ((MainLoggedActivity)getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ReservationsFragment()).commit();

                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addTransaction(View view, float amount) {
        String userId = user_id;


        String URL = "http://46.101.204.62/api/transaction";
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("user_id", userId);
            jsonBody.put("amount", amount);

            final String mRequestBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("LOG_VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG_VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    ((MainLoggedActivity)getActivity()).getUserAmount();
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
