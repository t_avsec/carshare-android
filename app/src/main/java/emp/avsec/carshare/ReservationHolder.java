package emp.avsec.carshare;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ReservationHolder extends RecyclerView.ViewHolder {
    TextView title, startDate, endDate;

    public ReservationHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title);
        startDate = itemView.findViewById(R.id.start_date);
        endDate = itemView.findViewById(R.id.end_date);

    }
}
