package emp.avsec.carshare;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReservationsFragment extends Fragment {
    private RequestQueue requestQueue;
    RecyclerView recyclerView;
    ReservationAdapter reservationAdapter;
    FirebaseAuth mAuth;
    ArrayList<ReservationModel> reservationModels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reservations, container, false);
        requestQueue = Volley.newRequestQueue(getActivity());
        mAuth = FirebaseAuth.getInstance();
        show_reservations(v.getContext());

        recyclerView =  v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        return v;
    }

    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject object = response.getJSONObject(i);

                    ReservationModel m = new ReservationModel();


                    int id = object.getInt("id");
                    int car_id = object.getInt("car_id");
                    String from = object.getString("from");
                    String to = object.getString("to");
                    int active = object.getInt("active");

                    m.setCar_id(car_id);
                    m.setId(id);
                    m.setStartDate(from);
                    m.setEndDate(to);


                    reservationModels.add(m);

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
            }

            reservationAdapter = new ReservationAdapter(getContext(), reservationModels);
            recyclerView.setAdapter(reservationAdapter);


        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("REST error", error.getMessage());
        }
    };

    public void show_reservations(Context c) {
        String url = "http://46.101.204.62/api/reservation/" + mAuth.getCurrentUser().getUid();
        JsonArrayRequest request = new JsonArrayRequest(url, jsonArrayListener, errorListener);
        requestQueue.add(request);

    }
}
